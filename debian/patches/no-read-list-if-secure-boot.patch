From b6708e1c7444199903cac458d66ae424ad33b159 Mon Sep 17 00:00:00 2001
From: Carlo Caione <carlo@endlessm.com>
Date: Fri, 14 Oct 2016 15:52:22 +0200
Subject: [PATCH] grub: Do not read list files when secure-boot is enabled

The list of all fs modules we ship is generated at compile time and
saved in a file called `fs.lst`. At boot time GRUB reads this file and
creates a list of available modules that is used (among others) by the
`autofs` module.

Unfortunately when we boot in EFI + secure-boot mode, even though the
modules are actually present in the prefix location, we cannot access
them since we cannot load any external module. This results in the
annoying error message:

    error: Secure Boot forbids loading module from (memdisk)/boot/...

With this patch we avoid reading the list files (including `fs.lst`) at
boot-time when we are in EFI + secure-boot mode.

Signed-off-by: Carlo Caione <carlo@endlessm.com>
---
 grub-core/normal/main.c | 11 +++++++++++
 1 file changed, 11 insertions(+)

diff --git a/grub-core/normal/main.c b/grub-core/normal/main.c
index 3cf30a27b..1b6ea2a1b 100644
--- a/grub-core/normal/main.c
+++ b/grub-core/normal/main.c
@@ -33,6 +33,9 @@
 #include <grub/charset.h>
 #include <grub/script_sh.h>
 #include <grub/bufio.h>
+#ifdef GRUB_MACHINE_EFI
+#include <grub/efi/efi.h>
+#endif
 
 GRUB_MOD_LICENSE ("GPLv3+");
 
@@ -236,6 +239,14 @@ grub_normal_init_page (struct grub_term_output *term,
 static void
 read_lists (const char *val)
 {
+#ifdef GRUB_MACHINE_EFI
+  if (grub_efi_secure_boot ())
+    {
+      grub_dprintf ("efi", "Secure Boot forbids loading modules. Not creating lists.\n");
+      return;
+    }
+#endif
+
   if (! grub_no_modules)
     {
       read_command_list (val);
-- 
2.26.2

